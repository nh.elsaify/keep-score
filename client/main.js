import React from 'react';
import ReactDom from 'react-dom';
import {Meteor} from 'meteor/meteor';
import { Players } from '../imports/api/players';
import {Tacker} from 'meteor/tracker';
import App from '../imports/ui/App'
import './main.html';

const renderPlayerList = (players) => {
  return players.map((player) => <Player player={player} key={player._id}/>)
}



Meteor.startup(() => {
  let players;
  Tracker.autorun(()=>{
    players = Players.find({},{sort:{score:-1}}).fetch();
    ReactDom.render(<App players={players} />,document.getElementById('app'))
})

});
