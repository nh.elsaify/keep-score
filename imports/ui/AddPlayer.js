import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { Players } from '../api/players';

export default class AddPlayer extends Component{
  constructor(props){
    super(props);
  }
  handleSubmit(e){
    e.preventDefault()
    const playerName = e.target.playerName.value;
    if( playerName){
      e.target.playerName.value = '';
      Players.insert({
        name: playerName,
        score: 0
      })
    }else{
      alert('plz enter player name')
    }
  }
  render(){
    return(
      <form onSubmit={this.handleSubmit.bind(this)}>
        <input name="playerName" placeholder="player name"/>
        <button>Submit</button>
      </form>
    )
  }
}
