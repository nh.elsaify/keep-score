import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { Players } from '../api/players';
import TitleBar from './TitleBar';
import AddPlayer from './AddPlayer';
import PlayerList from './PlayerList';
export default class App extends Component{
  constructor(props){
    super(props);
  }
  render(){
    let title = "score keeper"
    let name = 'bom';
    return(
      <div>
        <TitleBar title={title} createdby={name}/>
        <PlayerList players={this.props.players}/>
        <AddPlayer />
      </div>
    )
  }
}

App.propTypes = {
  players: PropTypes.array.isRequired,
}
