import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { Players } from '../api/players';
import Player from './Player';

export default class PlayerList extends Component{

  renderPlayerList(){
    if( this.props.players.length === 0 ){
      return <p>Add players</p>
    }else{
      return this.props.players.map((player)=><Player player={player} key={player._id}/>)
    }
  }
  render(){
    return(
      <div>
        {this.renderPlayerList()}
      </div>
    )
  }
}

PlayerList.propTypes = {
  players: PropTypes.array.isRequired,
}
