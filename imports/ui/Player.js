import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { Players } from '../api/players';

export default class Player extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <p>
        {this.props.player.name} has {this.props.player.score} point(s)
        <button onClick={()=>Players.update(this.props.player._id,{$inc: {score: -10}})}> - </button>
        <button onClick={()=>Players.update(this.props.player._id,{$inc: {score: +10}})}> + </button>
        <button onClick={()=>Players.remove({_id : this.props.player._id})}> X </button>
      </p>
    )
  }
}

Player.propTypes = {
  player: PropTypes.object.isRequired,
}
