import React,{ Component } from 'react';
import PropTypes from 'prop-types';

export default class TitleBar extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <div>
        <h1>{this.props.title}</h1>
        <p>Created By {this.props.createdby}</p>
      </div>
    )
  }
}

TitleBar.propTypes = {
  title: PropTypes.string.isRequired,
  createdby: PropTypes.string,
}
